FROM ubuntu:xenial

RUN apt-get update && \
    apt-get -y install python-pip

RUN pip install --upgrade pip && \
    pip install python-openstackclient